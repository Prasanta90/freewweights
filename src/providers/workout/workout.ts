import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, Headers } from "@angular/http";
import { Injectable } from '@angular/core';
import { CommonResponse } from "../../Model/common-response";
import { CommonProvider } from "../common/common";
//import { UpdateCustomerExercise } from '../../Model/update-customer-exercise';
/*
  Generated class for the WorkoutProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WorkoutProvider {

  public getworkoutplandetailsUrl: string = "/getworkoutplandetails";
  public datewisegetexercisedetailsUrl: string = "/datewisegetexercisedetails";
  public customerworkoutplanUrl: string = "/customerworkoutplan";
  public updatecustomerexerciseUrl: string = "/updatecustomerexercise";
  public getclubworkoutplansUrl: string = "/getclubworkoutplans";
  public getclubworkoutdetailsUrl: string = "/getclubworkoutdetails";
  public getclubworkoutexercisedetailsUrl: string = "/getclubworkoutexercisedetails";
  public getabsentdatesUrl: string = "/getabsentdates";
  public getcustomergoalUrl: string = "/getcustomergoal";
  public getgymoffdayUrl: string = "/getgymoffday";


  //updateCustomerExerciseList: UpdateCustomerExercise[] =[];

  constructor(public http: Http, public commonProvider: CommonProvider) {
    console.log('Hello WorkoutProvider Provider');
  }

  //get Goal name
  getGoalName(username: string) {
    var goal: any;
    goal = {
      "username": username
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getcustomergoalUrl, goal, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get workout count list for current week
  getWorkoutPlanDetails(username: string) {
    var workout: any;
    workout = {
      "username": username
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getworkoutplandetailsUrl, workout, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get exercise details for current date
  getDatewiseExerciseDetails(username, superSetId, scheduledDate, setSeqId, exerciseId) {
    var exercise: any;
    exercise = {
      "username": username,
      "superSetId": superSetId,
      "scheduleDate": scheduledDate,
      "setSeqId": setSeqId,
      "exerciseId": exerciseId
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log("To send ::::"+JSON.stringify(exercise));
    return this.http.post(this.commonProvider.hostUrl + this.datewisegetexercisedetailsUrl, exercise, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get my workout plan list
  getCustomerWorkoutPlan(username: string, selectedDate: Date) {
    var workout: any;
    workout = {
      "username": username,
      "byDate": selectedDate
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.customerworkoutplanUrl, workout, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //post updated exercise setails
  postUpdatedCustomerExercise(updateCustomerExerciseObj) {
    console.log("updateCustomerExerciseObj in postUpdatedCustomerExercise()&&&&&&" + JSON.stringify(updateCustomerExerciseObj))
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.updatecustomerexerciseUrl, updateCustomerExerciseObj, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get workout details for selected date
  getWorkoutDetailByDate(username: string, selectedDate: any) {
    var workout: any;
    workout = {
      "username": username,
      "selectedDate": selectedDate
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getworkoutplandetailsUrl, workout, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  getClubWorkoutPlanList(username: string){
    var clubWorkout: any;
    clubWorkout = {
      "username": username,
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getclubworkoutplansUrl, clubWorkout,  options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  getClubWorkoutDetails(planId: number){
    var plan: any;
    plan = {
      "planId" : planId
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getclubworkoutdetailsUrl, plan, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  getClubWorkoutExerciseDetails( superSetId, setSeqId, exerciseId, planId, forDay) {
    var exercise: any;
    exercise = {
      "superSetId": superSetId,
      "setSeqId": setSeqId,
      "exerciseId": exerciseId,
      "planId": planId,
      "forDay": forDay
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log("To send ::::"+JSON.stringify(exercise));
    return this.http.post(this.commonProvider.hostUrl + this.getclubworkoutexercisedetailsUrl, exercise, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get absent date list
  getAbsentList(username: string){
    var user: any;
    user = {
      "username": username,
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getabsentdatesUrl, user,  options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get gym off day
  getGymOffDay(gymId: number){
    var id: any;
    id = {
      "gymId": gymId,
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getgymoffdayUrl, id,  options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

}
