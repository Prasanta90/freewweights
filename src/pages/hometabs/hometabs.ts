import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { TodaySWorkoutPage } from '../today-s-workout/today-s-workout';
import { TodaySWorkoutNewPage } from '../today-s-workout-new/today-s-workout-new';
// import { TrackProgressPage } from '../track-progress/track-progress';
import { MyGymPage } from '../my-gym/my-gym';
import { NutritionPlanPage } from '../nutrition-plan/nutrition-plan';
import { ClubWorkoutPage } from '../club-workout/club-workout';
import { MyCalenderPage } from '../my-calender/my-calender';
import { NotificationsPage } from '../notifications/notifications';
import { Myprofile} from '../../Model/my-profile';
import { MyProgressPage } from '../my-progress/my-progress';
import { MembershipPlanPage } from '../membership-plan/membership-plan';


/**
 * Generated class for the HometabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-hometabs',
  templateUrl: 'hometabs.html'
})
export class HometabsPage {

  user: Myprofile;

  //todaySWorkoutPage = TodaySWorkoutPage
  todaySWorkoutPage = TodaySWorkoutNewPage
  //trackProgressRoot = TrackProgressPage
  trackProgressRoot = MyProgressPage
  myGymRoot = MyGymPage
  viewNutritionPlanRoot = NutritionPlanPage
  clubWorkoutsRoot = ClubWorkoutPage
  myCalenderPage = MyCalenderPage
  notificationsPage = NotificationsPage
  membershipPlanPage = MembershipPlanPage;

  todaySWorkoutParams: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user=this.navParams.get('user');
    console.log("I'm in HometabsPage:::::::User Details:::::"+JSON.stringify(this.user));
    this.todaySWorkoutParams = this.user;
  }

}
