
export class PackageDetail {
    serviceId: number;
    discountAllowedFlag: string;
    discountType?: any;
    maxDiscount: number;
    serviceStatus: string;
    remarks: string;
    serviceCost: number;
    serviceDesc: string;
    serviceDuration: string;
    serviceName: string;
    serviceType: string;
    gymId: number;
    serviceDeleted: string;
    serviceCostAfterDiscount: number;
    isDiscountValueZero: boolean;
    isDisablePackage: boolean;
}