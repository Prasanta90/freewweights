import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { MyProgressPage } from '..//my-progress/my-progress';

import { TodaySWorkoutNewPage} from '../today-s-workout-new/today-s-workout-new';
import { MyCalenderPage } from '../my-calender/my-calender';

/**
 * Generated class for the MyProgressTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-progress-tabs',
  templateUrl: 'my-progress-tabs.html',
})
export class MyProgressTabsPage {

  progressRoot = MyProgressPage
  todaySWorkoutPage = TodaySWorkoutNewPage
  myCalendersRoot = MyCalenderPage
  

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProgressTabsPage');
  }

}
