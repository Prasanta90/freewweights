import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  AlertController,
  LoadingController,
  ActionSheetController,
  Events
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from "moment";
import { Camera, CameraOptions } from "@ionic-native/camera";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { CommonProvider } from "../../providers/common/common";
import { CommonResponse } from "../../Model/common-response";
import { Myprofile } from "../../Model/my-profile";
import { MstGoal } from "../../Model/mst-goal";
import { MstTrainerModel } from "../../Model/mst-trainer";
// import { Country } from "../../Model/countries";

import { MyProfileProvider } from "../../providers/my-profile/my-profile";

import { ExpandableComponent } from "../../components/expandable/expandable";
import { MstService } from "../../Model/mst-service";
import { Subscription } from "../../Model/subscription";
// import { jsonpCallbackContext } from "@angular/common/http/src/module";

/**
 * Generated class for the MyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: "page-my-profile",
  templateUrl: "my-profile.html"
})
export class MyProfilePage {
  @ViewChild("firstName") myFirstName;

  editMyProfilePage: FormGroup;


  username: string;
  loader: any;
  trainerFirstNm: string;
  trainerLastNm: string;
  trainerName: string;
  countries: any[] = [];
  states: any[] = [];
  selectedCountryName: any;
  selectedCountry: any;
  country: any;
  state: any;
  selectedStateName: any;
  healthCond: any;

  selectedImage: string;
  imageURI: string;
  defaultImageUrl: string = "/assets/imgs/64-64.png";
  customerImageUrl: string;

  response: CommonResponse;
  user: Myprofile;
  userObj: Myprofile;
  mstGoalObj: MstGoal;
  mstTrainerObj: MstTrainerModel;
  subscriptionObj: Subscription;
  subscriptionList: Subscription[] = [];
  subscriptionListToView: Subscription[] = [];
  mstServiceObj: MstService;

  addressAccordionExpanded: boolean = false;
  otherInfoAccordionExpanded: boolean = false;
  trainerInfoAccordionExpanded: boolean = false;
  packageInfoAccordionExpanded: boolean = false;
  isButtonDisable: boolean = false;

  isReadOnlyFirstName: boolean = true;
  isReadOnlyLastName: boolean = true;
  isReadOnlyDOB: boolean = true;
  isReadOnlyContactNumber: boolean = true;
  isReadOnlyEmail: boolean = true;
  isReadOnlyAddressLine1: boolean = true;
  isReadOnlyAddressLine2: boolean = true;
  //isReadOnlyCity: boolean = true;
  //isReadOnlyState: boolean = true;
  //isReadOnlyPinCode: boolean = true;
  //isReadOnlyCountry: boolean = true;
  isReadOnlyEmergencyContactName: boolean = true;
  isReadOnlyEmergencyContactNo: boolean = true;
  isReadOnlyIntendedHourInGym: boolean = true;
  //isReadOnlyCustomerHealthCondition: boolean = true;
  isReadOnlyImportantNote: boolean = true;
  isReadOnlyCustomerHealthConditionDetails: boolean = true;
  isDisabledSaveButton: boolean = true;
  isShownCustomerHealthConditionDeatailField: boolean = false;
  isEditableCustomerHealthConditionDeatail: boolean = false;

  //regex
  textPattern: string = "^[A-Za-z ]*$";
  noPattern: string = "^(0|[1-9][0-9]*)$";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";



  customerHealthConditionList: any[] = [
    {
      id: 1,
      name: "None"
    },
    {
      id: 2,
      name: "Diabetes"
    },
    {
      id: 3,
      name: "Thyroid"
    },
    {
      id: 4,
      name: "PCOS"
    },
    {
      id: 5,
      name: "Cholesterol"
    },
    {
      id: 6,
      name: "Physical injury"
    },
    {
      id: 7,
      name: "Hyper tension"
    },
    {
      id: 10,
      name: "Others"
    }
  ];

  validation_messages = {
    firstName: [
      { type: "required", message: "You must enter first name." },
      {
        type: "pattern",
        message: "Event name should be composed of characters."
      },
      {
        type: "maxlength",
        message: "You can enter a name of maximum 20 characters."
      }
    ],
    lastNm: [
      { type: "required", message: "You must enter last name." },
      {
        type: "pattern",
        message: "Event name should be composed of characters."
      },
      {
        type: "maxlength",
        message: "You can enter a name of maximum 20 characters."
      }
    ],
    contactNo: [
      { type: "required", message: "Please enter your contact number." },
      {
        type: "pattern",
        message: "Phone number should be composed of digits."
      },
      { type: "maxlength", message: "Phone number should be 10 digit number." },
      { type: "minlength", message: "Phone number should be 10 digit number." }
    ],
    emailId: [
      { type: "required", message: "You must enter your emailid." },
      { type: "pattern", message: "Please enter a valid emailid." }
    ],
    addrLine1: [{ type: "required", message: "You must enter your address." }],
    // 'addrLine2': [
    //   { type: 'required', message: 'Please choose venue.' },
    // ],
    cityNm: [{ type: "required", message: "You must enter your city." }],
    stateNm: [
      { type: 'required', message: 'You must enter your state name.' },
    ],
    pinCode: [
      { type: "required", message: "Please enter pin code." },
      {
        type: "pattern",
        message: "Pin code number should be composed of digits."
      },
      { type: "maxlength", message: "Pin code should be 6 digit number." },
      { type: "minLength", message: "Pin code should be 6 digit number." }
    ],
    countryNm: [
      { type: 'required', message: 'You must enter your country name.' },
    ],
    emergencyContactNo: [
      { type: 'required', message: 'You must enter emergency contact number.' },
      { type: "maxlength", message: "Phone number should be 10 digit number." },
      { type: "minlength", message: "Phone number should be 10 digit number." }
    ],
    // customerHealthRemarks: [
    //   { type: "required", message: "You must type customer health condition details." }
    // ],
    emergency_contact_name: [
      { type: 'required', message: 'You must enter emergency contact name.' },
      { type: "pattern", message: "Event name should be composed of characters."},
      { type: "maxlength", message: "You can enter a name of maximum 40 characters."}
    ]
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private transfer: FileTransfer,
    private camera: Camera,
    public toastCtrl: ToastController,
    public commonProvider: CommonProvider,
    public myProfileProvider: MyProfileProvider,
    public alertCtrl: AlertController,
    public activityLoader: LoadingController,
    private actionSheetCtrl: ActionSheetController,
    public events: Events
  ) {

    //this.getCountryAndState();


    //this.username=this.navParams.get('username');
    this.username = localStorage.getItem("username");

    this.getCustomerInfo(this.username);

    this.editMyProfilePage = this.formBuilder.group({
      firstNm: [
        "",
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ],
      lastNm: [
        "",
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(20)
        ]
      ],
      dateOfBirth: [''],
      emailId: [
        "",
        [Validators.required, Validators.pattern(this.emailPattern)]
      ],
      contactNo: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10),
          Validators.minLength(10)
        ]
      ],
      customerStatus: [''],
      mstGoal: [""],
      gender: [""],
      addrLine1: ["", [Validators.required]],
      addrLine2: [""],
      cityNm: ["", [Validators.required]],
      stateNm: ['', [Validators.required]],
      pinCode: [
        "",
        [
          Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(6),
          Validators.minLength(6)
        ]
      ],
      countryNm: ['', Validators.required],

      emergencyContactNo: ['',
       [
        Validators.required,
          Validators.pattern(this.noPattern),
          Validators.maxLength(10),
          Validators.minLength(10)
      ]],
      emergency_contact_name: ['',
        [
          Validators.required,
          Validators.pattern(this.textPattern),
          Validators.maxLength(40)
        ]],
      hoursInGym: [""],
      customerHealthCondition: [""],
      remarks: [''],
      //customerHealthRemarks: ["", [Validators.required]],
      customerHealthRemarks: [''],
      trainerName: ['']
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyProfilePage");
  }


  //edit details
  editFirstName() {
    this.isReadOnlyFirstName = false;
    this.isDisabledSaveButton = false;
  }

  editLastName() {
    this.isReadOnlyLastName = false;
    this.isDisabledSaveButton = false;
  }
  editDOB() {
    this.isReadOnlyDOB = false;
    this.isDisabledSaveButton = false;
  }
  editContactNumber() {
    this.isReadOnlyContactNumber = false;
    this.isDisabledSaveButton = false;
  }
  editEmail() {
    this.isReadOnlyEmail = false;
    this.isDisabledSaveButton = false;
  }
  editAddressLine1() {
    this.isReadOnlyAddressLine1 = false;
    this.isDisabledSaveButton = false;
    this.isReadOnlyAddressLine2 = false;
    //this.isDisabledSaveButton = false;
  }
  // editAddressLine2() {
  //   this.isReadOnlyAddressLine2 = false;
  //   this.isDisabledSaveButton = false;
  // }
  // editCity() {
  //   this.isReadOnlyCity = false;
  //   this.isDisabledSaveButton = false;
  // }
  // editState() {
  //   this.isReadOnlyState = false;
  //   this.isDisabledSaveButton = false;
  // }
  // editPinCode() {
  //   this.isReadOnlyPinCode = false;
  //   this.isDisabledSaveButton = false;
  // }
  // editCountry() {
  //   this.isReadOnlyCountry = false;
  //   this.isDisabledSaveButton = false;
  // }
  editEmergencyContactName() {
    this.isReadOnlyEmergencyContactName = false;
    this.isDisabledSaveButton = false;
    this.isReadOnlyEmergencyContactNo = false;
    //this.isDisabledSaveButton = false;

    console.log("this.isReadOnlyEmergencyContactNo::::"+ this.isReadOnlyEmergencyContactNo);
  }
  // editEmergencyContactNo() {
  //   this.isReadOnlyEmergencyContactNo = false;
  //   this.isDisabledSaveButton = false;
  // }
  editIntendedHourInGym() {
    this.isReadOnlyIntendedHourInGym = false;
    this.isDisabledSaveButton = false;
  }

  editImportantNote() {
    this.isReadOnlyImportantNote = false;
    this.isDisabledSaveButton = false;
  }
  editCustomerHealthConditionDetails() {
    this.isReadOnlyCustomerHealthConditionDetails = false;
    this.isDisabledSaveButton = false;
  }

  toggleAddressAccordion() {
    this.addressAccordionExpanded = !this.addressAccordionExpanded;
  }

  toggleOtherInfoAccordion() {
    this.otherInfoAccordionExpanded = !this.otherInfoAccordionExpanded;
  }

  toggleTrainerInfoAccordion() {
    this.trainerInfoAccordionExpanded = !this.trainerInfoAccordionExpanded;
  }

  togglePackageInfoAccordion() {
    this.packageInfoAccordionExpanded = !this.packageInfoAccordionExpanded;
  }



  selectedCustomerhealthCond() {
    var customerHealthCondition = this.editMyProfilePage.controls['customerHealthCondition'].value;
    console.log("customerHealthCondition>>>>" + customerHealthCondition);
    var custHealthRemarks = this.editMyProfilePage.controls['customerHealthRemarks'].value;
    console.log("custHealthRemarks::::****"+custHealthRemarks);
    // if ((customerHealthCondition == 10) && (!this.editMyProfilePage.controls['customerHealthRemarks'].value)) {
    if (customerHealthCondition == 10) {
      this.isShownCustomerHealthConditionDeatailField = true;
      this.editMyProfilePage.controls["customerHealthRemarks"].setValue(null);
      if ((customerHealthCondition == 10) && (!custHealthRemarks)) {
        this.showAlert("Alert", "Please enter health condition details.");
        this.isEditableCustomerHealthConditionDeatail = false;
      }
      console.log("custHealthRemarks in if::::****"+this.editMyProfilePage.controls['customerHealthRemarks'].value);
    }
    else {
      this.isShownCustomerHealthConditionDeatailField = false;
      this.editMyProfilePage.controls["customerHealthRemarks"].setValue(null);
      console.log("custHealthRemarks in else::::****"+this.editMyProfilePage.controls['customerHealthRemarks'].value);

    }
  }

  getCustomerInfo(username) {
    this.presentLoading();
    this.commonProvider.getCustomerDetails(username).then(res => {
      this.dismissLodaing();
      this.response = res;
      console.log("Selected profile info:::::"+JSON.stringify(this.response));
      this.user = this.response.responseObj.myprofile;
      console.log("After login: " + JSON.stringify(res));
      if (this.user != null && this.response.status == 200) {
        this.events.publish('user:created', this.user);
        // this.editMyProfilePage.controls['username'].setValue(this.user.username);
        this.editMyProfilePage.controls['firstNm'].setValue(this.user.firstNm);
        this.editMyProfilePage.controls['lastNm'].setValue(this.user.lastNm);
        if (this.user.firstNm != null && this.user.lastNm != null) {
          localStorage.setItem('firstNm', this.user.firstNm);
          localStorage.setItem('lastNm', this.user.lastNm);
        }

        if (this.user.customerImage != null) {
          this.customerImageUrl = "data:image/png;base64," + this.user.customerImage;
          localStorage.setItem('customerImage', this.user.customerImage);

        }
        this.editMyProfilePage.controls['dateOfBirth'].setValue(this.user.dateOfBirth);
        this.editMyProfilePage.controls['emailId'].setValue(this.user.emailId);
        this.editMyProfilePage.controls['contactNo'].setValue(this.user.contactNo);
        this.editMyProfilePage.controls['customerStatus'].setValue(this.user.customerStatus);
        this.editMyProfilePage.controls['mstGoal'].setValue(this.user.mstGoal.goalNm);
        this.editMyProfilePage.controls['gender'].setValue(this.user.gender);
        if(this.user.gender != null && this.user.gender != ""){
          localStorage.setItem('gender', this.user.gender);
          console.log("this.user.gender:::::"+this.user.gender);
        }
        this.editMyProfilePage.controls['addrLine1'].setValue(this.user.addrLine1);
        this.editMyProfilePage.controls['addrLine2'].setValue(this.user.addrLine2);
        this.editMyProfilePage.controls['cityNm'].setValue(this.user.cityNm);
        this.editMyProfilePage.controls['stateNm'].setValue(this.user.stateNm);
        this.editMyProfilePage.controls['pinCode'].setValue(this.user.pinCode);
        this.editMyProfilePage.controls['countryNm'].setValue(this.user.countryNm);
        this.editMyProfilePage.controls['emergencyContactNo'].setValue(this.user.emergencyContactNo);
        this.editMyProfilePage.controls['emergency_contact_name'].setValue(this.user.emergency_contact_name);
        this.editMyProfilePage.controls['hoursInGym'].setValue(this.user.hoursInGym);
        //this.editMyProfilePage.controls['customerHealthCondition'].setValue(this.user.customerHealthCondition);
        this.getSelectedCustomerHealthCondition(this.user.customerHealthCondition);
        this.editMyProfilePage.controls['remarks'].setValue(this.user.remarks);
        this.editMyProfilePage.controls['customerHealthRemarks'].setValue(this.user.customerHealthRemarks);

        //if ((this.user.mstTrainer !== null) && (this.user.mstTrainer.firstNm !== null || this.user.mstTrainer.firstNm !== '') && (this.user.mstTrainer.lastNm !== null || this.user.mstTrainer.lastNm !== '')) {

        this.trainerFirstNm = this.user.mstTrainer.firstNm;

        this.trainerLastNm = this.user.mstTrainer.lastNm;
        if (this.trainerFirstNm && this.trainerLastNm) {
          this.trainerName = this.trainerFirstNm.concat(' ').concat(this.trainerLastNm);
          this.editMyProfilePage.controls['trainerName'].setValue(this.trainerName);
        }
        //this.trainerName = this.trainerFirstNm.concat(' ').concat(this.trainerLastNm);
        //this.editMyProfilePage.controls['trainerName'].setValue(this.trainerName);
        //}


        this.subscriptionList = this.user.subscription;
        for(let subs of this.subscriptionList){
          this.subscriptionObj = new Subscription();
          this.mstServiceObj =  new MstService();
          if(subs.mstService.serviceType === 'Product'){
            this.mstServiceObj.serviceName = subs.mstService.serviceName;
            this.subscriptionObj.mstService = this.mstServiceObj;
            this.subscriptionObj.startDate = '';
            this.subscriptionObj.endDate = '';
          }
          else{
            this.mstServiceObj.serviceName = subs.mstService.serviceName;
            this.subscriptionObj.mstService = this.mstServiceObj;
            this.subscriptionObj.startDate = subs.startDate;
            this.subscriptionObj.endDate = subs.endDate;
          }
          this.subscriptionListToView.push(this.subscriptionObj);
        }

        console.log("this.subscriptionList to view::::" + JSON.stringify(this.subscriptionListToView));
        console.log("this.subscriptionList" + JSON.stringify(this.user.subscription));


      }
      this.isReadOnlyFirstName = true;
      this.isReadOnlyLastName = true;
      this.isReadOnlyDOB = true;
      this.isReadOnlyContactNumber = true;
      this.isReadOnlyEmail = true;
      this.isReadOnlyAddressLine1 = true;
      this.isReadOnlyAddressLine2 = true;
      //this.isReadOnlyCity = true;
      //this.isReadOnlyState = true;
      //this.isReadOnlyPinCode = true;
      //this.isReadOnlyCountry = true;
      this.isReadOnlyEmergencyContactName = true;
      this.isReadOnlyEmergencyContactNo = true;
      this.isReadOnlyIntendedHourInGym = true;
      //this.isReadOnlyCustomerHealthCondition = true;
      this.isReadOnlyImportantNote = true;
      this.isReadOnlyCustomerHealthConditionDetails = true;
    });

    if (this.isReadOnlyFirstName && this.isReadOnlyLastName && this.isReadOnlyDOB && this.isReadOnlyContactNumber
      && this.isReadOnlyEmail && this.isReadOnlyAddressLine1 && this.isReadOnlyAddressLine2 && this.isReadOnlyEmergencyContactName
      && this.isReadOnlyEmergencyContactNo && this.isReadOnlyIntendedHourInGym && this.isReadOnlyImportantNote &&
      this.isReadOnlyCustomerHealthConditionDetails) {
      this.isDisabledSaveButton = true;
    }


  }


  compareHealthCond(x: any, y: any) {
    if (x.id == y.id) return true;
    else return false;
  }
  getSelectedCustomerHealthCondition(customerHealthCondition) {
    console.log("Customer health condition::::" + customerHealthCondition);
    for (let cusomertHealthCond of this.customerHealthConditionList) {
      if (cusomertHealthCond.id == customerHealthCondition) {
        console.log("Selected customerHealthCondition in getSelectedCustomerHealthCondition()>>>>>" + JSON.stringify(cusomertHealthCond));
        //this.healthCond = cusomertHealthCond;
        this.editMyProfilePage.controls["customerHealthCondition"].setValue(cusomertHealthCond.id);
        if (cusomertHealthCond.id == 10) {
          this.isShownCustomerHealthConditionDeatailField = true;
          this.isEditableCustomerHealthConditionDeatail = true;
        }
        else {
          this.isShownCustomerHealthConditionDeatailField = false;
          console.log("I'm in else::::");
        }
      }
    }
    this.isDisabledSaveButton = false;
  }

  editMyProfilePageEvent(data) {
    console.log("data:::::" + JSON.stringify(data));
    this.userObj = new Myprofile();
    this.mstGoalObj = new MstGoal();
    this.mstTrainerObj = new MstTrainerModel();


    // if (this.editMyProfilePage.controls['contactNo'].value > 0) {
    //   if (this.editMyProfilePage.controls['contactNo'].value == 10) {

    this.userObj.customerId = this.user.customerId;
    this.userObj.username = this.user.username;
    this.userObj.oldpassword = this.user.oldpassword;
    this.userObj.password = this.user.password;
    this.userObj.emailId = this.editMyProfilePage.controls['emailId'].value;
    this.userObj.addrLine1 = this.editMyProfilePage.controls['addrLine1'].value;
    this.userObj.addrLine2 = this.editMyProfilePage.controls['addrLine2'].value;
    this.userObj.cityNm = this.editMyProfilePage.controls['cityNm'].value;

    this.userObj.contactNo = this.editMyProfilePage.controls['contactNo'].value;

    this.userObj.countryNm = this.editMyProfilePage.controls['countryNm'].value;
    this.userObj.createBy = this.user.createBy;
    //this.userObj.emergency_contact_name = this.editMyProfilePage.controls['emergencyContactName'].value;
    this.userObj.createDate = this.user.createDate;
    //this.userObj.customerHealthCondition = data.customerHealthCondition.id;
    this.userObj.customerHealthCondition = this.editMyProfilePage.controls['customerHealthCondition'].value;
    //this.userObj.customerImage = null;
    this.userObj.customerStatus = this.editMyProfilePage.controls['customerStatus'].value;
    this.userObj.dateOfBirth = this.editMyProfilePage.controls['dateOfBirth'].value;
    this.userObj.emergencyContactNo = this.editMyProfilePage.controls['emergencyContactNo'].value;
    this.userObj.emergency_contact_name = this.editMyProfilePage.controls['emergency_contact_name'].value;
    this.userObj.firstNm = this.editMyProfilePage.controls['firstNm'].value;
    this.userObj.lastNm = this.editMyProfilePage.controls['lastNm'].value;
    this.userObj.gender = this.editMyProfilePage.controls['gender'].value;
    this.userObj.hoursInGym = this.editMyProfilePage.controls['hoursInGym'].value;
    // if (this.user.customerImage != null) {
    //   this.userObj.customerImage = this.user.customerImage;
    // }

    this.userObj.personalTrainerFlag = this.user.personalTrainerFlag;
    this.userObj.pinCode = this.editMyProfilePage.controls['pinCode'].value;
    this.userObj.remarks = this.editMyProfilePage.controls['remarks'].value;
    this.userObj.stateNm = this.editMyProfilePage.controls['stateNm'].value;
    this.userObj.trainerId = this.user.trainerId;
    
    if (this.userObj.customerHealthCondition == 10) {
      this.userObj.customerHealthRemarks = this.editMyProfilePage.controls['customerHealthRemarks'].value;
    }
    else{
      this.editMyProfilePage.controls['customerHealthRemarks'].setValue(null);
      this.userObj.customerHealthRemarks = this.editMyProfilePage.controls['customerHealthRemarks'].value; 
    }
    this.userObj.gymId = this.user.gymId;
    this.userObj.customerGoalId = this.user.customerGoalId;

    this.mstGoalObj.gymId = this.user.mstGoal.gymId;
    this.mstGoalObj.goalNm = this.user.mstGoal.goalNm;
    this.mstGoalObj.goalId = this.user.mstGoal.goalId;
    this.mstGoalObj.goalDesc = this.user.mstGoal.goalDesc;

    this.userObj.mstGoal = this.mstGoalObj;

    this.mstTrainerObj = this.user.mstTrainer;


    this.userObj.mstTrainer = this.mstTrainerObj;

    console.log("this.userObj*****"+JSON.stringify(this.userObj));
    this.postProfileInfo();

  }


  postProfileInfo() {
    this.presentLoading();
    this.myProfileProvider.editMyProfile(this.userObj).then(res => {
      this.response = res;
      if (
        this.response.status == 200 &&
        this.response.responseObj.editprofile == 1
      ) {
        this.dismissLodaing();
        this.showAlert("Success", "Data saved successfully.");
        this.isDisabledSaveButton = true;
        this.getCustomerInfo(this.username);
      } else {
        this.dismissLodaing();
        this.showAlert("Error", "Data not saved.");
      }
      //this.dismissLodaing();
    });
  }

  //Activity Indicator
  presentLoading() {
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  getImage() {
    console.log("I'm in getImage()");
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: "Use Library",
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Capture Image",
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
    // this.loader = this.activityLoader.create({
    //   content: "Loading...",
    //   // duration: 3000,
    //   spinner: "ios",
    //   dismissOnPageChange: true //dismiss can be also used
    // });
  }

  getPicture(sourceType: any) {
    //this.presentLoading();
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      allowEdit: false,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {
      this.presentLoading();
      console.log("imageData*****" + imageData);
      this.selectedImage = `data:image/jpeg;base64,${imageData}`;

      this.user.customerImage = imageData;
      this.uploadCustomerImage(this.username, this.user.customerImage);
      this.dismissLodaing();

      console.log(" this.user.customerImage:::::");
      console.log(this.user.customerImage);

      //this.recognizeImage();
    });
  }

  //To upload customer image

  uploadCustomerImage(username, customerImage) {
    //this.presentLoading();
    this.myProfileProvider
      .editCustomerImage(username, customerImage)
      .then(res => {
        this.response = res;
        if (this.response.status == 200 && this.response.message == "success") {
          this.dismissLodaing();
          this.showAlert("Success", "Image saved successfully.");
          this.getCustomerInfo(this.username);
        } else {
          this.dismissLodaing();
          this.showAlert("Error", "Image not saved.");
        }
        this.dismissLodaing();
      });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ["OK"]
    });
    alert.present();
  }
}
