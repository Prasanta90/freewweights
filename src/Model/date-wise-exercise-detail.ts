export class DateWiseExerciseDetail {
    exerciseId: number;
    exerciseName: string;
    exerciseType: string;
    particulars: string;
    guideline: string;
    level: string;
    musclegroup: string;
    equipmentName: string;
    equipmentDesc: string;
    bodyPartsName: string;
    bodyPartsDesc: string;
    exerciseParam1: string;
    exerciseParam1Uom: string;
    exerciseParam2: string;
    exerciseParam2Uom: string;
    exerciseParam3: string;
    exerciseParam3Uom: string;
    videoLink: string;
    exerciseImage: string;
    muscleImpactImage: string;
    ytkey: string;
}