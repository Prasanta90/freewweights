export class Invoice {
    invoice_id: number;
    invoice_date: string;
    invoice_category: string;
    invoice_due_date: string;
    invoice_desc: string;
    service_id: string;
    invoice_gross_amt: string;
    discount_amt: string;
    gst: string;
    invoice_net_amt: string;
    invoice_email_flag: string;

}