import { HttpClient } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { Http, RequestOptions, Headers } from "@angular/http";
import { Injectable } from '@angular/core';
import { CommonResponse } from "../../Model/common-response";
import { CommonProvider } from "../common/common";
import { CustomerAssessment } from '../../Model/customer-assessment';
import 'rxjs/add/operator/map';

/*
  Generated class for the MyProgressProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyProgressProvider {

  public getCustomerAssessmentUrl: string = "/getcustomerassessment";
  public savecustomerassessmentUrl: string = "/savecustomerassessment";
  public getcustomerassessmentdatesUrl: string = "/getcustomerassessmentdates";
  public getcustomerassessmentgraphdataUrl: string = "/getcustomerassessmentgraphdata";
  // public getcustomerassessmentdatesUrl: string = "http://192.168.1.137:8091/gymadminrest/mobile/restservice/getcustomerassessmentdates";


  dateInfo: CommonResponse;
  dateList: any[] = [];
  username: string;
  getcustomerassessmentgraphdata: any;
 


  constructor(public http: Http, public commonProvider: CommonProvider, public alertCtrl: AlertController) {
    console.log('Hello MyProgressProvider Provider');
    this.username = localStorage.getItem('username');
    //this.getPastAssessmentdateList();

  }

  // //To get past assessments date list
  // getPastAssessmentdateList() {
  //   var assessment: any;
  //   assessment = {
  //     "username": this.username
  //   };

  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   this.http
  //     .post(this.commonProvider.hostUrl + this.getcustomerassessmentdatesUrl, assessment, options)
  //     .map((res: any) => {
  //       return res.json();
  //     })
  //     .subscribe(
  //       (data: any) => {
  //         this.dateInfo = data;
  //         console.log("this.dateList::::::::::::::::::::::::::::::" + JSON.stringify(this.dateInfo));
  //         this.dateList = this.dateInfo.responseObj.getcustomerassessment;
  //         for (let d of this.dateList) {
  //           console.log(d);
  //         }
  //       },
  //       (err: any) => console.error("loadAllDates: ERROR"),
  //       () => console.log("loadAllDates: always")
  //     );
  // }

  //To get past assessments date list
  getPastAssessmentdateList(username: string) {
    var assessment: any;
    assessment = {
      "username": username
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getcustomerassessmentdatesUrl, assessment, options).toPromise()
      // return this.http.post(this.getcustomerassessmentdatesUrl, assessment, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get customar last assessment details
  getLastAssessment(username: string) {
    var assessment: any;
    assessment = {
      "username": username
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getCustomerAssessmentUrl, assessment, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //save updated customar assessment details
  saveAssessmentInfo(username: string, customerAssessmentObj: CustomerAssessment) {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.savecustomerassessmentUrl, customerAssessmentObj, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get customar assessment details for a specific date
  getLastAssessmentForSelectedDate(username: string, sDate: string) {
    var assessment: any;
    assessment = {
      "username": username,
      "byDate": sDate
    };

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl + this.getCustomerAssessmentUrl, assessment, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //to get values for plotting graph
  getCustomerAssessmentGraphData(customerAssessmentObj: CustomerAssessment) {

      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.commonProvider.hostUrl + this.getcustomerassessmentgraphdataUrl, customerAssessmentObj, options).toPromise()
        .then(res => <CommonResponse>res.json());
    

  }


  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


}
