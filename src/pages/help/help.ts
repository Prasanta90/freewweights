import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  faq: Array<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.faq = [
      { id: 1, question: "How do I add or remove a Favorite Workout?", description: "To add a favorite workout video, click the heart icon for that video on the My Favorites page, Video Listing page or on the Video Detail page. To remove a favorite, click on the heart icon from the My Favorites page, Video Listing, My Favorites or Video Detail page. This will make the heart be empty. Once the page is refreshed the video will be removed." },
      { id: 2, question: "Where is My Calendar?", description: "From the My Menu, select My Calendar." },
      { id: 3, question: "Where are my Purchased Programs?", description: "From the My Fitness Menu, select Purchased Programs." },
      { id: 4, question: "Can I download videos to watch offline?", description: "No" },
      

    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

}
