import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TodaySWorkoutNewPage } from '../today-s-workout-new/today-s-workout-new';
import { MyCalenderPage } from '../my-calender/my-calender';
import { MyProgressPage } from '../my-progress/my-progress';

/**
 * Generated class for the MembershipPlanTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-membership-plan-tabs',
  templateUrl: 'membership-plan-tabs.html',
})
export class MembershipPlanTabsPage {

  todaySWorkoutPage = TodaySWorkoutNewPage
  trackProgressRoot = MyProgressPage
  myCalenderPage = MyCalenderPage
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembershipPlanTabsPage');
  }

}
