import { LoginPage } from './../pages/login/login';
import { Notification } from './../Model/notification';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform, MenuController, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { App } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

// import { LoginPage } from '../pages/login/login';
import { SlidesPage } from '../pages/slides/slides';
// import { MyCalenderPage } from '../pages/my-calender/my-calender';
// import { TrackProgressPage } from '../pages/track-progress/track-progress';
// import { NutritionPlanPage } from '../pages/nutrition-plan/nutrition-plan';
// import { ClubWorkoutPage } from '../pages/club-workout/club-workout';
// import { GymClassPage } from '../pages/gym-class/gym-class';
// import { MyGymPage } from '../pages/my-gym/my-gym';
// import { AccountSettingPage } from '../pages/account-setting/account-setting';
// import { AboutUsPage } from '../pages/about-us/about-us';
import { LogoutPage } from '../pages/logout/logout';
import { HometabsPage } from '../pages/hometabs/hometabs';
// import { TodaySWorkoutPage } from '../pages/today-s-workout/today-s-workout';
import { MyProfileTabsPage } from '../pages/my-profile-tabs/my-profile-tabs';
import { MyProgressTabsPage } from '../pages/my-progress-tabs/my-progress-tabs';
// import { InboxPage } from '../pages/inbox/inbox';
import { ChangePasswordPage } from '../pages/change-password/change-password';
// import { MyProfilePage } from '../pages/my-profile/my-profile';
import { CommonProvider } from '../providers/common/common';
import { CommonResponse } from '../Model/common-response';
import { Myprofile } from '../Model/my-profile';
// import { TodaySWorkoutNewPage } from '../pages/today-s-workout-new/today-s-workout-new';
// import { MyProgressPage } from '../pages/my-progress/my-progress';
import { MembershipPlanPage } from '../pages/membership-plan/membership-plan';
// import { MembershipPlanTabsPage } from '../pages/membership-plan-tabs/membership-plan-tabs';
import { MyCalendarTabsPage } from '../pages/my-calendar-tabs/my-calendar-tabs';
import { HelpPage } from '../pages/help/help';
// import { MyWorkoutPlanPage } from '../pages/my-workout-plan/my-workout-plan';
// import { MyWorkoutPlanTodayPage } from '../pages/my-workout-plan-today/my-workout-plan-today';
import { MyWorkoutPlanTodayTabsPage } from '../pages/my-workout-plan-today-tabs/my-workout-plan-today-tabs';
// import { MealPlanForCurrentDatePage } from '../pages/meal-plan-for-current-date/meal-plan-for-current-date';
// import { PaymentsDuesPage } from '../pages/payments-dues/payments-dues';
import { MealPlanTodayTabsPage } from '../pages/meal-plan-today-tabs/meal-plan-today-tabs';
// import { AngularFireModule } from 'angularfire2';
// import { AngularFirestoreModule } from 'angularfire2/firestore';
// import { Firebase } from '@ionic-native/firebase';
import { FcmService } from './../fcm.service';
import { PaymentHistoryTabsPage } from '../pages/payment-history-tabs/payment-history-tabs';


@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rememberme = localStorage.getItem('rememberme');
  //rootPage: any = LoginPage;
  rootPage: any;
  //rootPage: any = SlidesPage;


  username: string;
  //emailId: string = 'abc@gmail.com';
  isMyProfilePage: boolean = true;
  //emailId: string;
  response: CommonResponse;
  user: Myprofile;
  customerImage: string;
  defaultImageUrl: string = "/assets/imgs/64-64.png";
  customerImageUrl: string;
  customerName: string;
  firstNm: string;
  lastNm: string;
  memberSince: any;
  alertShown: boolean = false;
  deviceType: string;
  isDeviceTypeAndroid: string;
  isDeviceTypeIos:string


  pages: Array<{ title: string, component: any, icon: string, data: any }>;

  ngOnInit() {

  }

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    public commonProvider: CommonProvider, public menu: MenuController, public alertCtrl: AlertController,
    public app: App, public fcm: FcmService, public events: Events, public plt: Platform,private toastCtrl: ToastController) {
    this.events.subscribe('user:created', (user) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome', user);
      this.firstNm = user.firstNm;
      this.lastNm = user.lastNm;
      if ((this.firstNm != null && this.firstNm != '') && (this.lastNm != null && this.lastNm != '')) {
        this.customerName = this.firstNm.concat(' ').concat(this.lastNm);
      }

      this.username = user.username;
      this.customerImage = user.customerImage;
      this.customerImageUrl = "data:image/png;base64," + this.customerImage;
      //this.customerImageUrl = localStorage.getItem('customerImageUrl');
      this.memberSince = user.createDate;
    });


    console.log("this.customerImageUrl:::::" + this.customerImageUrl);

    console.log("I'm in app.component");
    console.log("username in app.component is::::::*****" + this.username);

    console.log("this.customerName######" + this.customerName);
    this.initializeApp();

    if (this.rememberme) {
      // this.rootPage = TodaySWorkoutNewPage;
      console.log("I'm in if block when notification is tapped");
      this.rootPage = HometabsPage;
      //this.rootPage = LoginPage;
    }
    // else if(this.username !== undefined && this.username !== null && this.username !== ""){
    //   //this.rootPage = SlidesPage;
    //   this.rootPage = LoginPage;

    // }
    else {
      console.log("I'm in else block when notification is tapped");
      this.rootPage = SlidesPage;
    }

    


    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Fitness Arena', component: HometabsPage, icon: "menu_home", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Fitness Arena' } },
      //{ title: 'My Workout Plan', component: MyWorkoutPlanTodayPage, icon: "menu_workouts", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Workout Plan' } },
      { title: 'My Workout Plan', component: MyWorkoutPlanTodayTabsPage, icon: "menu_workouts", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Workout Plan' } },
      // { title: 'My Workout Plan', component: HometabsPage, icon: "menu_workouts", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Fitness Arena'} },
      //{ title: 'Meal Plan', component: HometabsPage, icon: "menu_nutrition", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Meal Plan'} },
      // { title: 'My Meal Plan', component: MealPlanForCurrentDatePage, icon: "menu_nutrition", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Meal Plan' } },
      { title: 'My Meal Plan', component: MealPlanTodayTabsPage, icon: "menu_nutrition", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Meal Plan' } },
      { title: 'My Calendar', component: MyCalendarTabsPage, icon: "menu_calendar", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Calendar' } },
      // { title: 'My Progress', component: TrackProgressPage, icon: "menu_progress", data:{'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Progress'} },
      { title: 'My Progress', component: MyProgressTabsPage, icon: "menu_progress", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Progress' } },
      { title: 'My Profile', component: MyProfileTabsPage, icon: "menu_profile", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Profile' } },
      { title: 'Club Workout Plan', component: HometabsPage, icon: "menu_club", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Club Workouts' } },
      { title: 'Services', component: MembershipPlanPage, icon: "menu_membership", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Membership Plan' } },
      { title: 'Payment History', component: PaymentHistoryTabsPage, icon: "payments", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'My Profile' } },
      { title: 'Help', component: HelpPage, icon: "menu_help", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Help' } },
      { title: 'Change Password', component: ChangePasswordPage, icon: "menu_changePassword", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Change Password' } },
      { title: 'Logout', component: LogoutPage, icon: "menu_logout", data: { 'username': this.username, 'firstNm': this.firstNm, 'lastNm': this.lastNm, menuTitle: 'Logout' } },


    ];
  }

  
  presentToast(message,title) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top',
      // showCloseButton: true,
      // closeButtonText: 'OK'
    });
  
    // toast.onDidDismiss(() => {
    //   console.log('Dismissed toast');
    // });
    toast.onDidDismiss((data, role) => {    
      console.log('Dismissed toast');
      if (role== "close") {
        console.log('Dismissed toast yes'+this.rememberme);
         //put back the item on right place
        //  if (this.rememberme) {
          // this.nav.setRoot(HometabsPage);
        // }
        // else {
        //   this.nav.setRoot(LoginPage);
        // }
      }
  });
  
    toast.present();
  }

  initializeApp() {

    console.log("I'm in initializeApp");
    //var fcmToken: any;
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.notificationSetup();
      //fcmToken = this.fcm.getToken();
      //console.log("fcm token issss::::"+JSON.stringify(fcmToken));
      //this.fcm.getToken();
      //localStorage.setItem("fcm_token", fcmToken);
      
      this.platform.registerBackButtonAction(() => {
        if (this.alertShown == false) {
          this.presentConfirm();
        }
      }, 0)
    });

  }
   notificationSetup() {
    console.log("I'm in notificationSetup");
    this.fcm.getToken();
    console.log("I'm in app.cpmponent fcm");
    // this.fcm.onNotifications().subscribe(
    //   (msg) => {
    //     console.log("msg::::"+msg);
    //     if (this.platform.is('android')) {
    //     this.presentToast(msg.aps.alert);
    //       //console.log(msg.aps.alert);
    //     } else {
    //       this.presentToast(msg.body);
    //       console.log("-------" + msg.body);
    //     }
    //   },(error)=>{
    //     console.log("error---" + error);
    //   });
    this.fcm.onNotifications().subscribe(data => {
      if(data.wasTapped){
        console.log("Received in background");
        //alert( JSON.stringify(data) );
        this.presentToast(data.body,data.title);
      } else {
        console.log("Received in foreground");
        //alert( JSON.stringify(data));
        //console.log(data)
        this.presentToast(data.body,data.title);
      }
    });
  }

  //Ionic Back Button Confirm Before Exit
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Exit',
      message: 'Do you want Exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown = false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            //localStorage.clear();
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present().then(() => {
      this.alertShown = true;
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    console.log("Page::::::::" + JSON.stringify(page));
    this.nav.setRoot(page.component, {
      user: page.data
    });
  }


}
