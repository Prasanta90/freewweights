export class MstService {
    serviceId: number;
    discountAllowedFlag: string;
    discountType: string;
    maxDiscount: number;
    serviceStatus: string;
    remarks: string;
    serviceCost: number;
    serviceDesc: string;
    serviceDuration: string;
    serviceName: string;
    serviceType: string;
}