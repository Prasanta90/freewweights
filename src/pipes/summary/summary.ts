import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SummaryPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'summary',
})
export class SummaryPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    if(!value)
      return null;
    else if(value.length < 20)
      return value;
    else
    return value.substr(0, 40) +  '...';
    //return value.toLowerCase();
    // return value.toLowerCase();
  }
}
