export class WorkoutPlanCount {
    totalWorkoutPlan: number;
    completedWorkoutPlan: number;
    //incompletionPercentage: number;
    completionPercentage: number;
    scheduleDate: string;
    workoutImg: string;
}