import { Component, ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Slides } from 'ionic-angular';

/**
 * Generated class for the SlidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-slides',
  templateUrl: 'slides.html',
})
export class SlidesPage {
  @ViewChild(Slides) slides: Slides;
  slidesList: any[] = [];
  currentIndex: number;
  nextIndex: number = 0;

  thirdSlideDescription: string = "\"A little extra knowledge is not bad\", connect with us and know more on our services and offers for you.";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.slidesList = [
      {
       // title: "Welcome to the Docs!",
        description: "Crafted workout and nutrition plans for you to get you going in the gym.",
        image: "assets/imgs/SliderOne.png",
      },
      {
        //title: "What is Ionic?",
        description: "Track your progress, performance and assessment to make your every effort count",
        image: "assets/imgs/SliderTwo.png",
      },
      // {
      //  // title: "What is Ionic Cloud?",
      //  // description: "\"A little extra knowledge is not bad\", connect with us and know more on our services and offersfor you.",
      //  // image: "assets/imgs/SliderThree.png",
      // }
    ];
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SlidesPage');
  }

  slideChanged() {
    this.currentIndex = this.slides.getActiveIndex();
    this.nextIndex = this.currentIndex;
    console.log('Current index is', this.currentIndex);
  }

  goToSlide() {
    this.nextIndex += 1;
    console.log("this.nextIndex:::::"+this.nextIndex);
    this.slides.slideTo(this.nextIndex, 500);
  }

  goToLoginPage(){
    this.navCtrl.push(LoginPage,{
     
    });
  }

}
