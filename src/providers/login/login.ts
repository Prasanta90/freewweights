import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, Headers } from "@angular/http";
import { Injectable } from '@angular/core';
import { CommonResponse } from "../../Model/common-response";
import { CommonProvider } from "../common/common";


/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {
  
  public loginUrl: string = "/login";
  public changepasswordUrl: string = "/changepassword";
  public forgotpasswordUrl: string = "/resetpasswordonotp";
  public getemailidonforgotpasswordUrl: string = "/forgotpassword";


  constructor(public http: Http, public commonProvider: CommonProvider) {
    console.log('Hello LoginProvider Provider');
  }

    //login
    login(username: string, password: string, userToken:string, deviceType: string) {
      var login: any;
      login= { 
      "username":username,
      "password":password,
      "userToken":userToken,
      "deviceType": deviceType
      };
      console.log("Login data::::"+JSON.stringify(login));
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.commonProvider.hostUrl+this.loginUrl, login, options).toPromise()
      .then(res => <CommonResponse>res.json());
      }
  //   login(username: string, password: string) {
  //     var login: any;
  //     login= { 
  //         "username":username,
  //         "password":password,

  //       };
  //     console.log(login);
  //     let headers = new Headers({ 'Content-Type': 'application/json' });
  //     let options = new RequestOptions({ headers: headers });
  //     return this.http.post(this.commonProvider.hostUrl+this.loginUrl, login, options).toPromise()
  //         .then(res => <CommonResponse>res.json());
  // }

  //change password
  changeMyPassword(username: string, oldpassword: string ,password: string) {
    var changePass: any;
    changePass= { 
        "username":username,
        "oldpassword": oldpassword,
        "password":password
      };
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl+this.changepasswordUrl, changePass, options).toPromise()
        .then(res => <CommonResponse>res.json());
}

//forgot password
forgotPassword(username: string, password: string) {
  var forgotPass: any;
  forgotPass= { 
      "username":username,
      "password":password
    };
  
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.forgotpasswordUrl, forgotPass, options).toPromise()
      .then(res => <CommonResponse>res.json());
}

checkRegisteredEmail(username: string) {
  var forgotPass: any;
  forgotPass= { 
      "username":username,
    };
  
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.getemailidonforgotpasswordUrl, forgotPass, options).toPromise()
      .then(res => <CommonResponse>res.json());
}



}
