import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, Headers } from "@angular/http";
import { Injectable } from '@angular/core';
import { CommonResponse } from "../../Model/common-response";
import { PackageDetail } from '../../Model/package-details';
import { HttpErrorResponse } from '@angular/common/https';
import { jsonpCallbackContext } from '@angular/common/http/src/module';



/*
  Generated class for the CommonProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommonProvider {

  dateArray = new Array();
  fcm_token: any;
  countryAndState: any;
  //packageDetail: PackageDetail;
  //public hostUrl: string = "http://192.168.1.118:8091/gymadminrest/mobile/restservice";
  //public hostUrl: string = "http://192.168.1.137:8091/gymadminrest/mobile/restservice";
  //public hostUrl: string = "http://182.73.216.93:8091/gymadminrest/mobile/restservice";
  //public hostUrl: string = "http://182.73.216.92:8091/gymadminrest/mobile/restservice";
  //public hostUrl: string = "http://13.232.154.59:8080/gymadminrest/mobile/restservice";
  public hostUrl: string = "http://182.73.216.92:8080/gymadminrest/mobile/restservice";

  public myprofileUrl: string = "/myprofile";
  public getcustomerperformanceforweekUrl: string = "/getcustomerperformanceforweek";
  public getallnotificationUrl: string = "/getallnotification";
  public deletenotificationUrl: string = "/deletenotification";

  constructor(public http: Http) {
    console.log('Hello CommonProvider Provider');


    // this.http.get('./assets/gistfile1.json').subscribe(
    //   data => {
    //     console.log("Country and states:::" + JSON.stringify(data.countries));
    //   });
  }

  //get customer information
  getCustomerDetails(username: string) {
    var email: any;
    email = {
      "username": username,
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.hostUrl + this.myprofileUrl, email, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  getCustomerPerformanceForWeek(username: string) {
    var email: any;
    email = {
      "username": username,
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.hostUrl + this.getcustomerperformanceforweekUrl, email, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  getCustomerPerformanceForDayWise(username: string, byDate: string) {
    var performance: any;
    performance = {
      "username": username,
      "byDate": byDate
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log("performance datewise send data::::"+JSON.stringify(performance));
    return this.http.post(this.hostUrl + this.getcustomerperformanceforweekUrl, performance, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }


  //   //get country and state details
  getCountryAndState() {
    return this.http.get('./assets/gistfile1.json').toPromise()
      .then(res => <CommonResponse>res.json());
  }

  //get all notification
  getAllNotification(username: string) {
    var user: any;
    user = {
      "username": username,
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.hostUrl + this.getallnotificationUrl, user, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }


  //to delete notification
  deleteNotification(customerId: string, notificationId: string, notificationDate: string, customerNotificationId: number) {
    var custId: any;
    custId = {
      "customerId": customerId,
      "notificationId": notificationId,
      "notificationDate": notificationDate,
      "customerNotificationId": customerNotificationId
    };
    console.log("Cust id for deletion notification::::"+JSON.stringify(custId));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.hostUrl + this.deletenotificationUrl, custId, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }

  getAllNotificationByDateRange(username: string, createDate: string, byDate: string) {
    var search: any;
    search = {
      "username": username,
      "createDate": createDate,
      "byDate": byDate
    };
    console.log("search notification::::"+JSON.stringify(search));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.hostUrl + this.getallnotificationUrl, search, options).toPromise()
      .then(res => <CommonResponse>res.json());
  }




}
