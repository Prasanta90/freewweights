import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { LoginPage } from '../../pages/login/login';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { Myprofile } from '../../Model/my-profile';
import { LoginProvider } from '../../providers/login/login';
import { HometabsPage } from '../hometabs/hometabs';


/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  changePasswordForm: FormGroup;
  username: string;
  // firstNm: string;
  // lastNm: string;
  // name: string;
  password: string;
  confirmPassword: string;
  response: CommonResponse;
  user: Myprofile;
  loader: any;

  noPattern: string = "^(0|[1-9][0-9]*)$";
  //passwordPattern: string = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$";
  passwordPattern: string = "^(?=.*?[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$";

  validation_messages = {
    'oldpassword': [
      { type: 'required', message: 'Please enter your current password.' },
      { type: 'pattern', message: 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character.' },
     { type: "minlength", message: "Password should contain minimum 8 characters." },
      //{ type: "maxlength", message: "Password should contain maximum 10 characters." }
      //{ type: 'pattern', message: 'Password should be composed of digits.' },
      //{ type: 'minLength', message: 'Password should be 6 digit number.' }
    ],
    'password': [
      { type: 'required', message: 'Please enter your new password.' },
      { type: 'pattern', message: 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character.' },
     { type: "minlength", message: "Password should contain minimum 8 characters." },
      //{ type: "maxlength", message: "Password should contain maximum 10 characters." }
     // { type: 'pattern', message: 'Password should be composed of digits.' },
     // { type: 'minLength', message: 'Password should be 6 digit number.' }
    ],
    'confirmPassword': [
      { type: 'required', message: 'Please enter your new password.' },
      { type: 'pattern', message: 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character.' },
     { type: "minlength", message: "Password should contain minimum 8 characters." },
      //{ type: "maxlength", message: "Password should contain maximum 10 characters." }
     // { type: 'pattern', message: 'Password should be composed of digits.' },
     // { type: 'minLength', message: 'Password should be 6 digit number.' }
    ]

  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public commonProvider: CommonProvider, public alertCtrl: AlertController, public loginProvider: LoginProvider,
    public activityLoader: LoadingController) {
    this.username = localStorage.getItem('username');
    // this.getCustomerInfo(this.username);


    this.changePasswordForm = this.formBuilder.group({
     // 'oldpassword': ['', [Validators.required, Validators.pattern(this.noPattern), Validators.minLength(6)]],
     'oldpassword': ['', [Validators.required, Validators.pattern(this.passwordPattern), Validators.minLength(8)]],
     'password': ['', [Validators.required, Validators.pattern(this.passwordPattern), Validators.minLength(8)]],
     'confirmPassword': ['', [Validators.required, Validators.pattern(this.passwordPattern), Validators.minLength(8)]],
     // 'password': ['', [Validators.required, Validators.pattern(this.noPattern), Validators.minLength(6)]],
     // 'confirmPassword': ['', [Validators.required, Validators.pattern(this.noPattern), Validators.minLength(6)]]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
  }


  /*getCustomerInfo(username) {
    this.commonProvider.getCustomerDetails(username).then(res => {
      this.response = res;
      console.log(JSON.stringify(this.response));
      this.user = this.response.responseObj.myprofile;
      console.log("After login: " + JSON.stringify(res));
      if (this.user != null && this.response.status == 200) {
        if(this.user.firstNm != null || this.user.lastNm != null){
          this.firstNm = this.user.firstNm;
          this.lastNm = this.user.lastNm;
          this.name = this.firstNm.concat(' ').concat(this.lastNm);
        console.log("I'm in TodaySWorkoutNewPage:::::::User Details:::::" + JSON.stringify(this.username));
        }
        
        
        
      }
    })
  }*/

  changePassword(data) {

    console.log("Change Password info::::" + JSON.stringify(data));
    if (data.password === data.confirmPassword) {
      this.presentLoading();
      this.loginProvider.changeMyPassword(this.username, data.oldpassword, data.password).then(res => {
        this.response = res;
        //this.user = this.response.responseObj.myprofile;
        //console.log("After login: "+JSON.stringify(res));
        if (this.response.status == 200 && this.response.message == '1') {
          this.dismissLodaing();
          this.showAlert("Success", this.response.responseObj.changePassword);
          this.navCtrl.push(HometabsPage, {

          });
        }
        else if (this.response.status == 200 && this.response.message == "0") {
          // this.showAlert("Error", "Could not match your previous password");
          this.dismissLodaing();
          this.showAlert("Error", this.response.responseObj.changePassword);
        }

      })
    }
    else {
      //this.dismissLodaing();
      this.showAlert("Error", "New password and confirm password are not same.")
      //this.changePasswordForm.reset()
    }

  }

  //Activity Indicator
  presentLoading() {
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
