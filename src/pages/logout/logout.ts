import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HometabsPage } from '../hometabs/hometabs';

//@IonicPage()
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
  }
  btnYes() {
    localStorage.clear();
    this.navCtrl.setRoot(LoginPage);
    localStorage.setItem('gender', null);
    localStorage.setItem('username', null);
    localStorage.setItem('password', null);
    localStorage.setItem('customerId', null);
    localStorage.setItem('gymId', null);
    localStorage.setItem('customerImage', null);
    localStorage.setItem('firstNm', null);
    localStorage.setItem('lastNm', null);
    localStorage.setItem('memberSince', null);
    localStorage.setItem('deviceTypeName', null);
    localStorage.setItem('isDeviceTypeAndroid', null);
    localStorage.setItem('isDeviceTypeIos', null);
    localStorage.setItem('fcm_token', null);
    localStorage.setItem('rememberme', null);
  }
  btnNo() {
    this.navCtrl.setRoot(HometabsPage);
  }

}
