import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, Platform } from 'ionic-angular';
import { WorkoutProvider } from '../../providers/workout/workout';
import { CommonProvider } from '../../providers/common/common';
import { CommonResponse } from '../../Model/common-response';
import { DateWiseExerciseDetail } from '../../Model/date-wise-exercise-detail';
import { SplashScreen } from '@ionic-native/splash-screen';


/**
 * Generated class for the MoreInfoAboutExerciseNextPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-more-info-about-exercise-next',
  templateUrl: 'more-info-about-exercise-next.html',
})
export class MoreInfoAboutExerciseNextPage {

  loader: any;
  //exerciseDescription: string = "The biceps is a muscle on the front part of the upper arm. The biceps includes a “short head” and a “long head” that work as a single muscle.The biceps is attached to the arm bones by tough connective tissues called tendons. The tendons that connect the biceps muscle to the shoulder joint in two places are called the proximal biceps tendons. The tendon that attaches the biceps muscle to the forearm bones  (radius and ulna) is called the distal biceps tendon. When the biceps contracts, it pulls the forearm up and rotates it outward";
  category: any;
  username: string;
  response: CommonResponse;
  isPresentExerciseParam1: boolean = false;
  exerciseImageUrl: string;
  defaultImageUrl: string = "/assets/imgs/no-image-found-360x260.png";
  

  dateWiseExerciseDetailObj: DateWiseExerciseDetail = new DateWiseExerciseDetail();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public workoutProvider: WorkoutProvider, public platform: Platform,
    public commonProvider: CommonProvider, public alertCtrl: AlertController,
    public modalCtrl: ModalController, public splashscreen: SplashScreen,
    public activityLoader: LoadingController) {

    // platform.ready().then(() => {
    //   this.splashscreen.hide();
    // });
    this.username = localStorage.getItem('username')
    this.category = this.navParams.get('category');
    console.log("I'm in workout-category-details next page" + JSON.stringify(this.category));

    this.getExerciseDetails(this.username, this.category.superSetId, this.category.scheduledDate, this.category.setSeqId, this.category.exerciseId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoreInfoAboutExerciseNextPage');
  }

  //to get exercise details
  getExerciseDetails(username, superSetId, scheduledDate, setSeqId, exerciseId) {
    this.presentLoading();
    this.workoutProvider.getDatewiseExerciseDetails(username, superSetId, scheduledDate, setSeqId, exerciseId).then(res => {
      this.response = res;
      this.dismissLodaing();
      console.log("exercise data::::::" + JSON.stringify(this.response));
      if (this.response.status == 200 && this.response.message === "success") {

        if (typeof this.response.responseObj.datewisegetexercisedetails !== 'undefined' && this.response.responseObj.datewisegetexercisedetails.length > 0) {
          for (let exerciseDtls of this.response.responseObj.datewisegetexercisedetails) {
            this.dateWiseExerciseDetailObj = exerciseDtls;
            console.log("this.dateWiseExerciseDetailObj:::::" + JSON.stringify(this.dateWiseExerciseDetailObj));
            if ((this.dateWiseExerciseDetailObj.exerciseParam1 != null && this.dateWiseExerciseDetailObj.exerciseParam1 != '') || (this.dateWiseExerciseDetailObj.exerciseParam2 != null && this.dateWiseExerciseDetailObj.exerciseParam2 != '') || (this.dateWiseExerciseDetailObj.exerciseParam3 != null && this.dateWiseExerciseDetailObj.exerciseParam3 != '')) {
              this.isPresentExerciseParam1 = true;
            }
            if (this.dateWiseExerciseDetailObj.muscleImpactImage != null) {
              this.exerciseImageUrl = "data:image/png;base64," + this.dateWiseExerciseDetailObj.muscleImpactImage;
            }
          }
        }

        else {
          this.showAlert("Alert", "Exercise for a current date is not present.");
          //this.dismissLodaing();
        }
      }

      // else {
      //   this.showAlert("Alert", "Exercise for a current date is not present.");
      //   //this.dismissLodaing();
      // }
    })
  }

  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  //Activity Indicator
  presentLoading() {
    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

}
